#include "Wifi.h"
#include "Var_Global/Var_Glob.h"
#include "Definition/Definition.h"
Wifi::Wifi()
{
}

void Wifi::Connection_Wifi()
{
  //Wifi, mode station
  WiFi.mode(WIFI_STA);
  WiFi.begin(SsidPA, MotDePassePA);

  while (WiFi.status() != WL_CONNECTED)
  {
    delay(1000);
    affDebug("En attente de connexion au réseau... \n");
  }
  affDebug("Connecté à ");
  affDebugln(SsidPA);
}


void Wifi::AffAdrrIP()
{
  //Adresse IP
  affDebug("IP address: ");
  affDebugln(WiFi.localIP());
}
void Wifi::AffAdrrMAC()
{
  //Adresse MAC
  affDebug("Mac address: ");
  affDebugln(WiFi.macAddress());
}

void Wifi::conversionMAC()
{
  //------------ADRESSE MAC--------------------------***
  //Récupération de l'adresse MAC
  WiFi.macAddress(RecupAdresseMac);

  //Conversion de l'adresse MAC en string
  for (int i = 0; i < 6; i++)
  {
    MessageMqtt += String(RecupAdresseMac[i], HEX) + ":";
  }
  MessageMqtt.toUpperCase();                    //Met l'adresse MAC en majuscule
  MessageMqtt.remove(MessageMqtt.length() - 1); //Retire le dernier ":"
  MessageMqtt.toCharArray(MessageAdrrMAC, 18);         //Conversion en char[]
}

void Wifi::reconnect()
{
  //  VerfiConnectionWifi();
  Connection_Wifi();
}

//Connection à la passerelle edge
// while (!client.connected())
// {
//  affDebug("Connexion à la passerelle Edge ...");
//  delay(500);
//  if (client.connect("EspClient_8266", MQTTUSER, MQTTPASSWORD))
//  {
//    affDebugln("Connecté");
//   delay(500);
//  }
// }*/