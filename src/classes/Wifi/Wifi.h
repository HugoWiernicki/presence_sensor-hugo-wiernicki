#include <ESP8266WiFi.h>
class Wifi
{
private:
  String MessageMqtt = "";
  byte RecupAdresseMac[6];
 
public:
  Wifi();
  void Connection_Wifi();
  void AffAdrrIP();
  void AffAdrrMAC();
  void conversionMAC();
  void reconnect();
};
