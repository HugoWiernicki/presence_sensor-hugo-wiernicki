class MesureDist
{     
public:
        MesureDist();                     //Constructeur
        ~MesureDist();                    //Destructeur
        void Capture_Distance(int prm);   //Calcul de distance/*int prmD*/
        void Seuil_a_Vide(int prmSeuil);  //Méthode qui définit le seuil à vide
        int LireDistance();               //Méthode qui lis la distance
        void ChangementPlace();
};
