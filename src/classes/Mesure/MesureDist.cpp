//******Liste des includes**********
#include "Definition/Definition.h"
#include "Var_Global/Var_Glob.h"
#include "MesureDist.h"
//Constructeur******************************
MesureDist::MesureDist()
{
}
//Destructeur******************************
MesureDist::~MesureDist()
{
}

//Calcul de distance***********************
void MesureDist::Capture_Distance(int prm)
{
    //Début de transmission
    //Donne l'adresse par défaut du capteur = (0xE0) i2c prend les 7 bits de poids fort (0x70)
    Wire.beginTransmission(0x70);
    //On pointe sur le registre de commande (0x00)
    Wire.write(byte(0x00));
    //commande pour meusurer la valeur en "centimetres" = (0x51), "pouces"(0x50)
    Wire.write(byte(0x51));
    //arrêt de transmission
    Wire.endTransmission();
    //La fiche technique suggere au moins 65 millisecondes
    delay(70);
    //Début de transmission, Donne l'adresse par défaut du capteur = (0x70)
    Wire.beginTransmission(0x70);
    //Met le pointeur du registre sur le registre écho n°1  (0x02)
    Wire.write(byte(0x02));
    //arrêt de transmission
    Wire.endTransmission();
    //Demande 2 octet au capteur
    Wire.requestFrom(0x70, 2);
    //si les 2 bits sont reçu
    if (2 <= Wire.available())
    {
        //recevoir un octet de poids fort (écrase la lecture précédente)
        Distance = Wire.read();
        //décalage de l'octet faible de 8 bits pour être l'octet de poids fort
        Distance = Distance << 8;
        Distance |= Wire.read();
  
    }
    //delai 2s pour lisibilité
    delay(2000);
}
//Calcul Seuil à vide*****************************
void MesureDist::Seuil_a_Vide(int prmSeuil)
{
    SeuilDistance = prmSeuil;
}
//Lecture distance***********************************
int MesureDist::LireDistance(){

    return Distance;
}

void MesureDist::ChangementPlace(){
     if (Distance >= SeuilDistance) //Si la distance est > ou = au seuil
    {
      affDebugln("Place libre\n"); //Place Libre
      EtatPlace = "Libre";
      digitalWrite(vert, HIGH); //Led verte on
      digitalWrite(rouge, LOW); //Led rouge off
    }
    else
    {
      affDebugln("Place occupé\n"); //Place occupé
      EtatPlace = "Occupe";
      digitalWrite(vert, LOW);   //Led verte off
      digitalWrite(rouge, HIGH); //Led rouge on
    }
}