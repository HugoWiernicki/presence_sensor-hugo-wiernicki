//-----------Include necessaire-----------------
#include "Definition/Definition.h"
#include "Var_Global/Var_Glob.h"
#include "Fonction/Mqtt_callbackR/MqttReciveID.h"
#include <FS.h>
//Chemin du fichier de configuration
#define PATH_FICHIER "data/Fichier_config.txt"
//Taille allouée pour le fichier de config 


class Spiffs
{
private:
//*******************Variable Spiffs******************//
 bool codErrSpiffs;
 JsonObject objCfgJson;     //obj json
 File monFichier; //Accés fichier
 String contenuFichierConf;  //fichier de conf ss forme d'un String
 StaticJsonDocument<350> jsonDocConf;  // Création de l’objet « JsonDocument » 
 DeserializationError error;     //Pour traitement des erreurs
public:
    Spiffs();
    ~Spiffs();
    String test;
    void LireOnSpiffs();
    void EcrireOnSpiffs();
};
