#include "Spiffs.h"

Spiffs::Spiffs()
{
  codErrSpiffs = false;
}
Spiffs::~Spiffs()
{
}
void Spiffs::EcrireOnSpiffs()
{
  codErrSpiffs = SPIFFS.begin(); // start de la spiffs
  if (codErrSpiffs == true)
  {
    monFichier = SPIFFS.open(PATH_FICHIER, "w"); //on ouvre en ecriture
    Serial.println("----ecriture----");
    //*** monFichier.write((uint8_t *)(RecupId.c_str()), RecupId.length());
    // Valeur ecrite
    jsonDocConf["ID"] = RecupId;
  }
  monFichier.close();
  //Démontage du systeme de fichiers
  SPIFFS.end();
}

void Spiffs::LireOnSpiffs()
{
  codErrSpiffs = SPIFFS.begin(); // start de la spiffs true si c'est ouvert
  if (codErrSpiffs == true)
  {
    monFichier = SPIFFS.open(PATH_FICHIER, "r"); //on ouvre en lecture
    if (monFichier.available())                  ///dispo et si c'est écrit dessus
    {
      Serial.println("--lecture--");

      //Deserialisation du contenu du fichier (String -> JSON)
      error = deserializeJson(jsonDocConf, contenuFichierConf);
      if (error)
      { //SI il y a une erreur:
        //Affichage de l'erreur
        Serial.println("Erreur de lecture du fichier");
        Serial.println(error.c_str());
      }
      else
      { //SI il n'y a pas d'erreur:
        //On récupère le fichier au format JSON sour forme d'objet JSON
        objCfgJson = jsonDocConf.as<JsonObject>();

        //L'objet JSON est pret à etre utilisé
        // initialisation des variables
        test = objCfgJson["ID"].as<String>();
        //Démontage du systeme de fichiers
        SPIFFS.end();
      }

      //On stocke le contenu du fichier dans un String
      contenuFichierConf = monFichier.readString();
      monFichier.close();
    }
  }
}