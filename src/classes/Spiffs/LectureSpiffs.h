//-----------Include necessaire-----------------
#include "Definition/Definition.h"
#include "Var_Global/Var_Glob.h"
#include <FS.h>
//-----------Definition necessaire--------------
#define FICHIER "data/Fichier_config.txt"

//extern bool codErrSpiffs;
//extern String motdepasse;
//extern File monFichier; //Accés fichier
void LireOnSpiffs()
{
    codErrSpiffs = SPIFFS.begin(); // start de la spiffs
    if (codErrSpiffs == true)
    {
        //Lecture d'un fichier de test + affichage de son contenu
        affDebugln("Spiffs OK");
        affDebug("//--------------------------------------------------------------");
        affDebugln("Lecture du fichier");
        monFichier = SPIFFS.open(FICHIER, "r");

        if (monFichier.available()) ///dispo et si c'est écrit dessus
        {
            affDebugln("\tFichier dispo");
            //affDebugln(String("\t") + monFichier.readString());
            affDebug("-----------------------------------");
            motdepasse = monFichier.readString();
            affDebug(motdepasse);
            monFichier.close();
        }
        //Démontage du systeme de fichiers
        SPIFFS.end();
    }
}

void EcrireOnSpiffs()
{
    codErrSpiffs = SPIFFS.begin(); // start de la spiffs
                                   //Ecriture d'un texte dans le fichier de test
    affDebugln("//------------------------------------------------------------");
    affDebugln("Création d'un fichier");
    monFichier = SPIFFS.open(FICHIER, "w");

    affDebugln("\tFichier dispo");
    monFichier.write((uint8_t *)(motdepasse.c_str()), motdepasse.length());
    monFichier.close();
    SPIFFS.end();
}