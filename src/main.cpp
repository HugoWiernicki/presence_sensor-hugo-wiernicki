//Créer par Hugo Wiernicki, derniere modif 27/03/2020, Capteur_présence V7
//Le programme récupére des info pour se connecter au reseau par le spiffs
//Ensuite il se connecte et calcul une distance et envoi à la passerelle edge
//Si la place est occupé ou libre.
//*******************Listes des Includes****************************//
#include "Var_Global/Var_Glob.h" //Pour les variables global
#include "Fonction/Mqtt_callbackR/MqttReciveID.h" //Pour la fct callback
#include "classes/Mesure/MesureDist.h" //Pour mesurer la distance
#include "classes/Spiffs/Spiffs.h"  //Pour lecture spiffs
#include "classes/Wifi/Wifi.h"  //Pour le wifi
#include "Definition/Definition.h"  //Pour les defines
//************Definitions utile*************************************//

//----------Les différentes phases------------------
#define PhaseSC -1  //Phase sortie de carton
#define PhaseENR 0  //Phase Enregistrement du fichier de configuration
#define PhaseFONC 1 //Phase Fonctionnement

//Objets                                       
WiFiClient WifiClient;  //Obj WifiClient
PubSubClient client(MQTTSERVER, MQTTPORT, callbackR, WifiClient); //Obj client
Wifi objWifi;  //obj classe wifi
MesureDist ObjDist; //Objet pour accéder a la classe MesureDist
Spiffs objtest;  //obj classe spiffs
//**********Initialisation de l'application********************
void setup()
{
  //-----------Variables-----------//
  const int Bouton = D5;  //Btn pour envoyé @MAC
  int EtatBtn;  //Etat Btn
  int CodErrBtnAddrMac = -1;  //Code erreur btn @MAC

  //Start
  Serial.begin(115200); //On donne le nombre de bauds donc 115200 bits

  phase = PhaseSC; //Phase Sortie de carton
  affDebug("Le numéro de phase est : ");
  affDebugln(phase);

  //Broche D2 et D1 utilisé par le télémètre (SDA et SCL)
  Wire.begin(D2, D1);

  objWifi.Connection_Wifi();   //connection wifi
  objWifi.AffAdrrIP();     //Affiche @IP
  objWifi.AffAdrrMAC();    //Affiche @MAC
  objWifi.conversionMAC(); //Convertion de @MAC pour envoi vers BDD

  //Connection à la passerelle edge
  client.connect("EspClient_8266", MQTTUSER, MQTTPASSWORD);

  //Envoi @MAC selon l'état du btn---------------------------------
  while (CodErrBtnAddrMac == -1)
  {
    EtatBtn = digitalRead(Bouton);  //Lire le bouton 0 ou 1
    if (EtatBtn == 0)
    {
      CodErrBtnAddrMac = 0;
      affDebugln("Envoi de l'adresse Mac...");
      client.publish(EnvoiTopicAddrMAC, MessageAdrrMAC);  //publie @MAC
    }
    else
    {
      affDebugln("L'adresse Mac ne c'est pas envoye...");
    }
    delay(500);
  }
  //-------------------ID-------------------------***
  client.subscribe(RECIVE_ID);  //Abonnement au topic giveID

  affDebug("Attente de l'ID....\n");
  //Tant que l'ID n'est pas reçu
  while (coderreurID == -1)
  {
    client.loop();
    if (RecupId == "")
    {
      affDebugln("L'ID n'est pas connu...");
      delay(500);
    }
    else
    {
      coderreurID = 0;
      //Phase d'enregistrement (ID, Seuil) etc..
      phase = PhaseENR;
      affDebug("Le numéro de phase est : ");
      affDebugln(phase);
      //Quand l'ID est récupéré on écrit ce dernier dans la spiffs
      objtest.EcrireOnSpiffs();
      objtest.LireOnSpiffs();
    }
    delay(500);
  }

//Calcul 1 fois pour récuperer le seuil à vide
if (phase == PhaseENR)
{
  //appel la methode pour calculer la distance
  ObjDist.Capture_Distance(Distance); //Calcul la distance
  affDebug("La distance est de :");
  affDebugln(Distance);
  //appel la méthode pour calculer le seuil à vide
  ObjDist.Seuil_a_Vide(Distance); //fait le seuil à vide
  affDebug("Distance par défaut est : ");
  affDebugln(SeuilDistance);
}

phase = PhaseFONC; //phase fonctionnement
affDebug("Le numéro de phase est : ");
affDebugln(phase);
}
//Fin de la boucle Setup()
//******************Corps de l'application***********************
void loop()
{
  //----------variables---------------------//
  //pile recommandé pour les documents de moins de 1 Ko
  //Valeur 55 calculé sur l'assitance arduino en fonction de mon message
  StaticJsonDocument<155> EnvoiMqtt; //obj json
  String EtatPrecedent = ""; //Récupére l'etat précedent
  char message[100];  //Message pour envoyé les données
  pinMode(rouge, OUTPUT);  //LED rouge
  pinMode(vert, OUTPUT);  //LED verte

  while (phase == PhaseFONC)
  {
   // client.loop();
    //appel la methode pour calculer la distance
    ObjDist.Capture_Distance(Distance);

    //appel la méthode pour calculer le seuil à vide
    LireDistance = ObjDist.LireDistance();
    affDebug("La distance est de : ");
    affDebugln(LireDistance);

    itoa(Distance, message, 10); //Conversion int to string en base 10
    ObjDist.ChangementPlace();  //Methode qui dit si libre ou occupé

    //Compare si l'envoi d'un même état de place à déjà été publié
    if (EtatPrecedent != EtatPlace)
    {
      affDebugln("Mise à jour.");
      affDebug("Topic: ");
      affDebugln(OCCUPATION_PLACE);

      //Met l'ID et l'état place dans un tableau
      EnvoiMqtt["ID"] = RecupId;
      EnvoiMqtt["EtatPlace"] = EtatPlace;

      //On sérialise pour envoyé et déserialise pour lire
      serializeJson(EnvoiMqtt, message, 50); //50 = length
      client.publish(OCCUPATION_PLACE, message); //envoi MQTT à la passerelle
      delay(1000);
      EtatPrecedent = EtatPlace;  //Enregistre l'etat precedent
    }
    else
    {
      affDebug("Le changement d'etat de place a ete deja mise à jour pour ce type de place : ");
      affDebugln(EtatPlace);
    }
  }
}
