//***************Listes des Includes*******************//
#include <ESP8266WiFi.h>  //Connexion au wifi
#include <Wire.h>         //Capteur I2c
#include <PubSubClient.h> //Connexion au broker MQTT
#include <ArduinoJson.h>  //Pour le Json
//****************variables globales********************//
extern String SsidPA;       //"tpsnir";
extern String MotDePassePA; //"t2p0s2n0";

extern int phase;         //Contient les differentes situations de phase
extern int Distance;      //Contient la valeur de distance calculé
extern int SeuilDistance; //Contient le seuil de distance
extern int LireDistance;  //Contient la Distance à lire

extern char MessageAdrrMAC[12];  //Message pour envoi @MAC en mqtt
extern int coderreurID;   //Contient l'erreur pour l'ID de l'esp
extern String RecupId; //Contient l'ID de l'esp

extern String EtatPlace;  //Contient l'etat de la place libre ou occupé
extern const int rouge;   //LED rouge
extern const int vert;  //LED verte



