#define DEBUGe           // mode « Debug »
//#define RELEASE		// pour le mode « Release »

#ifdef DEBUGe                                  // pour le mode « Debug »
#define affDebugln(mess) Serial.println(mess) // aff message + retour à la ligne
#define affDebug(mess) Serial.print(mess)     // aff message sans retour ligne
#endif

#ifdef RELEASE           // pour le mode « Release »
#define affDebugln(mess) // pas d’aff
#define affDebug(mess)   // pas d’aff
#endif
