//********Liste des includes***************************
#include "Var_Global/Var_Glob.h"  //Pour les variables global
#include "Definition/Definition.h"
//-------------Fonction de Callback----------------------

//Récupere l'id de la passerelle edge
void callbackR(const char *topic, byte *payload, unsigned int length)
{
  for (unsigned int i = 0; i < length; i++)
  {
    RecupId += (char)payload[i];    //conversion de payload en char
  }
  affDebug("Le message est arrivé, sur le TOPIC : [");
  affDebug(topic);
  affDebug("] ");
  affDebug(" = ");
  affDebugln(RecupId);
}